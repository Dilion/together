#include"../inc/Board.h"

#ifndef __MAIN_H__
#define __MAIN_H__

class Gfunc
{
public:
	Gfunc();								//构造函数
	~Gfunc();								//析构函数
	int pub = 1;
	
	int main();		//胜负判断接口函数
	int NewThread();		//禁手判断接口函数
private:
	int pri = 0;
};

#endif